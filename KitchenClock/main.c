/*
 * KitchenClock.c
 *
 * Created: 2018. 05. 20. 22:37:11
 * Author : DANI
 */ 

#define F_CPU 2000000
#include <util/delay.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>

#include <string.h>
#include "Headers/uart.h"
#include "Headers/lcd.h"

#define STARTOFMINUTE = 1
#define STARTOFDATE	  = 0x100000

#define IDLE		0
#define YEARSET		1
#define MONTHSET	2
#define DAYSET		3
#define HOURSET		4
#define MINSET		5
#define SECSET		6
#define BUTTONSET	7

#define MSECOND_bm	0b0000001111111111
#define SECOND_bm	0b1111110000000000
#define MIN_bm		0b0000000000111111
#define HOUR_bm		0b0000011111000000
#define DAY_bm		0b1111100000000000
#define MONTH_bm	0b0000000000001111
#define YEAR_bm		0b1111111111110000

//DCF vel� �llapotg�p�hez
#define	DCFREADY		0x00
#define DCFENDMINUTE	0x01
#define DCFONE			0x02
#define DCFZERO			0x03
#define DCFINVALID		0x04
#define DCFSTART		0x05
#define DCFMINUTE		0x06
#define DCFHOURS		0x07
#define DCFDAYOFMONTH	0x08
#define DCFDAY			0x09
#define DCFMONTH		0x0A
#define DCFYEAR			0x0B

//Gombok
#define BTNIDLE			0b11000000
#define BTNRESTART		0b00000001
#define BTNTENSEC		0b00000010
#define BTNMINUTE		0b00000100
#define BTNTENMIN		0b00001000
#define BTNHOUR			0b00010000
#define BTNSTART		0b00100000
#define BTNLONG			0b10000000
#define BTNLONG1		0b10000010
#define BTNLONG2		0b10000100
#define BTNLONG3		0b10001000
#define BTNLONG4		0b10010000

//MAIN �llapotg�p
#define READY		0x01
#define TIMERSET	0x02
#define TIMESET		0x03
#define COUNTDOWN	0x04
#define BEEP		0x05

#define CR 0x0d;	//Kocsi vissza
#define LF 0x0a;	//�j sor

//Strukt�ra az id� t�rol�s�hoz
struct DateStruct {
	unsigned int yearMonth;		//�vek l�s h�napok t�rol�sa k�t byte-on
	unsigned int dayHourMin;	//Napok, �r�k �s percek t�rol�sa k�t byte-on
	unsigned int second;		//M�sodpercek �s 1/1024 m�sodpercek t�rol�sa k�t byte-on
	unsigned char dateSetState;	//Id� be�ll�t�s eset�n jelzi, hogy melyik �rt�ket �ll�tj�k
};

//Visszasz�ml�l�shoz strukt�ra
struct TimerDateStruct {	
	unsigned int sec:6;		//M�sodpercek 0-59
	unsigned int min:6;		//Percek 0-59
	unsigned int hour:4;	//�r�k	0-9
};

//Gombnyom�s �llapotok
struct ButtonPreset {
	struct TimerDateStruct btnTenSec;	//10 m�sodperc gomb
	struct TimerDateStruct btnMinute;	//1 perc gomb
	struct TimerDateStruct btnTenMin;	//10 perc gomb
	struct TimerDateStruct btnHour;		//1 �ra gomb
};

//Glob�lis �llapot flag-ek
struct Flag {
	unsigned int miliSecondFlag:1;		//Eltelt 1/1024-ed m�sodperc
	unsigned int secondFlag:1;			//Eltelt 1 m�sodperc
	unsigned int SPI_rdy:1;				//SPI v�tel befejez�d�tt
	unsigned int uartInputFlag:1;		//Soros porton adatot vett
	unsigned int dcfHighInputFlag:1;	//DCF magas szint id�m�r�s befejez�d�tt
	unsigned int dcfLowInputFlag:1;		//DCF alacsony szint id�m�r�s befejez�d�tt
};

struct Flag flag;

//Visszasz�ml�l�s be�ll�t�sa UART-on �rkezett adat alapj�n
void setTimerDateSet(struct TimerDateStruct* result, char* data) {
	result->hour = data[0]-48;						//�ra
	result->min	 = (data[2]-48)*10 + (data[3]-48);	//Perc
	result->sec  = (data[5]-48)*10 + (data[6]-48);	//M�sodperc
}

//Visszasz�ml�l�s strukt�ra m�sol�sa
void cpyTimerDate(struct TimerDateStruct* result, struct TimerDateStruct* souce) {
	result->hour = souce->hour;	//�ra
	result->min = souce->min;	//Perc
	result->sec = souce->sec;	//M�sodperc
}

//Soros adat v�tel�hez
struct uartState {
	unsigned char state;		//V�tel aktu�lis �llapota
	unsigned char inCounter;	//Vett byte-ok sz�ml�l�ja
	char textBuf[9]; 			//V�teli buffer
	unsigned int dateDate;		//Adat
};

//d�tum be�ll�t�sa.
struct DateStruct setDate(char pos, int data) {
	struct DateStruct GlobalDate;	//Eredm�ny v�ltoz�ja
	//Poz�ci� f�ggv�ny�ben v�laszt
	switch(pos) {
		case SECSET : GlobalDate.second = ((data & SECOND_bm) | GlobalDate.second); break;			//M�sodperc
		case MINSET : GlobalDate.dayHourMin = ((data & MIN_bm) | GlobalDate.dayHourMin); break;		//Perc
		case HOURSET : GlobalDate.dayHourMin = ((data & HOUR_bm) | GlobalDate.dayHourMin); break;	//�ra
		case DAYSET : GlobalDate.dayHourMin = ((data & DAY_bm) | GlobalDate.dayHourMin); break;		//Nap
		case MONTHSET : GlobalDate.yearMonth = ((data & MONTH_bm) | GlobalDate.yearMonth); break;	//H�nap
		case YEARSET : GlobalDate.yearMonth = ((data & YEAR_bm) | GlobalDate.yearMonth); break;		//�v
		default : {
			GlobalDate.yearMonth = 0x7c7b;	//1991.11
			GlobalDate.dayHourMin = 0x8800;	//17 00:00:00
			GlobalDate.second = 0;
			GlobalDate.dateSetState = IDLE;
		} break;
	}
	return GlobalDate;
}

//D�tum, �tv�lt�sa string-be
void dateToString(char* outDate, struct DateStruct GlobalDate) {
	if((GlobalDate.dateSetState == YEARSET) && ((GlobalDate.second >> 10)%2)) {
		outDate[0] =  ' ';
		outDate[1] =  ' ';
		outDate[2] =  ' ';
		outDate[3] =  ' ';
	}
	//�v �tv�lt�sa BCD ASCII karakterekk�
	else {
		outDate[0] =  (((GlobalDate.yearMonth >> 4) / 1000) + 48);				//Ezres helyi�rt�k
		outDate[1] =  ((((GlobalDate.yearMonth >> 4) / 100) % 10) + 48);		//Sz�zas helyi�rt�k
		outDate[2] =  (((((GlobalDate.yearMonth >> 4) / 10) % 10) % 10) + 48);	//Tizes helyi�rt�k
		outDate[3] =  (((GlobalDate.yearMonth >> 4) % 10) + 48);				//Egyes helyi�rt�k
	}
	outDate[4] = 46;	//Pont
	if((GlobalDate.dateSetState == MONTHSET) && ((GlobalDate.second >> 10)/2)) {
		outDate[5] =  ' ';
		outDate[6] =  ' ';
	}
	//H�nap �tv�lt�sa BCD ASCII karakterekk�
	else {
		outDate[5] = (((GlobalDate.yearMonth & MONTH_bm) / 10) + 48);	//Tizes helyi�rt�k
		outDate[6] = (((GlobalDate.yearMonth & MONTH_bm) % 10) + 48);	//Egyes helyi�rt�k
	}
	outDate[7] = 46;	//Pont
	if((GlobalDate.dateSetState == DAYSET) && ((GlobalDate.second >> 10)/2)) {
		outDate[8] =  ' ';
		outDate[9] =  ' ';
	}
	//Nap �tv�lt�sa BCD ASCII karakterekk�
	else {
		outDate[8] = (((GlobalDate.dayHourMin >> 11) / 10) + 48);	//Tizes helyi�rt�k
		outDate[9] = (((GlobalDate.dayHourMin >> 11) % 10) + 48);	//Egyes helyi�rt�k
	}
	outDate[10] = 32;	//Sz�k�z
	outDate[11] = 32;	//Sz�k�z
	if((GlobalDate.dateSetState == HOURSET) && ((GlobalDate.second >> 10)/2)) {
		outDate[12] =  ' ';
		outDate[13] =  ' ';
	}
	//�ra �tv�lt�sa BCD ASCII karakterekk�
	else {
		outDate[12] = ((((GlobalDate.dayHourMin & HOUR_bm) >> 6) / 10) + 48);	//Tizes helyi�rt�k
		outDate[13] = ((((GlobalDate.dayHourMin & HOUR_bm) >> 6) % 10) + 48);	//Egyes helyi�rt�k
	}
	outDate[14] = 58;	//Kett�spont
	if((GlobalDate.dateSetState == MINSET) && ((GlobalDate.second >> 10)/2)) {
		outDate[15] =  ' ';
		outDate[16] =  ' ';
	}
	//Perc �tv�lt�sa BCD ASCII karakterekk�
	else {
		outDate[15] = (((GlobalDate.dayHourMin & MIN_bm) / 10) + 48);	//Tizes helyi�rt�k
		outDate[16] = (((GlobalDate.dayHourMin & MIN_bm) % 10) + 48);	//Egyes helyi�rt�k
	}
	outDate[17] = 58;	//Kett�spont
	if((GlobalDate.dateSetState == SECSET) && ((GlobalDate.second >> 10)/2)) {
		outDate[18] =  ' ';
		outDate[19] =  ' ';
	}
	//M�sodperc �tv�lt�sa BCD ASCII karakterekk�
	else {
		outDate[18] = (((GlobalDate.second >> 10) / 10) + 48);	//Tizes helyi�rt�k
		outDate[19] = (((GlobalDate.second >> 10) % 10) + 48);	//Egyes helyi�rt�k
	}
	strcat(outDate, "\r\n\0");		//Lez�r� karakterek
}

//Visszasz�ml�l�si �rt�k �tv�lt�sa BCD ASCII string-be
void timerToString(char* outTimer, struct TimerDateStruct* timer) {
	outTimer[0] = timer->hour + 48;			//�ra
	outTimer[1]	= 58;						//Kett�spont
	outTimer[2] = (timer->min / 10) + 48;	//Perc tizes helyi�rt�l
	outTimer[3] = (timer->min % 10) + 48;	//Perc egyes helyi�rt�k
	outTimer[4] = 58;						//Kett�spont
	outTimer[5] = (timer->sec / 10)	+ 48;	//M�sodperc tizes helyi�rt�k
	outTimer[6] = (timer->sec % 10) + 48;	//M�sodperc egyes helyi�rt�k
	strcat(outTimer, "\r\n\0");				//Lez�r� karakterek
	
}

//Visszasz�ml�l�si �rt�k ki�r�sa az LCD kijelz�re
void writeTimerToLCD(struct TimerDateStruct* timer) {
	char outTimer[10] = "";				//Kimeneti string inicializ�l�sa
	timerToString(outTimer,timer);		//�rt�kek �tv�lt�sa BCD ASCII form�tumra
	writeLCDdataToPos(outTimer, 0x9A);	//LCD �r�s megfelel� poz�ci�t�l
}

//DCF77 debugol�s�hoz. Ha DCF perc szinkront vesz, azt ki�rja soros portra
void minuteMarker(struct DateStruct date) {
	char outDate[23] = "";						//�res string inicializ�l�sa
	char MinuteMarker[100] = "MinuteMarker  ";	//Kimeneti string inicializ�l�sa
	dateToString(outDate, date);				//D�tum �tv�lt�sa string-be
	strcat(MinuteMarker, outDate);				//Kimeneti string �s d�tum string �sszef�z�se
	writeTextUART(MinuteMarker);				//Ki�r�s soros portra
}

volatile int inSPI;
volatile char shiftSPI;

//H�m�r� szenzor olvas�sa
void readTMP() {
	inSPI = 0;					//El�z� SPI adat t�rl�se
	PORTD_OUTCLR = 0b00010001;	//Chip select
	SPID_INTCTRL = 0b00000011;	//D porton l�v� SPI interrupt enged�lyez�se
	flag.SPI_rdy = 0;			//SPI �rv�nytelen adat
	shiftSPI = 8;				//Seg�dv�ltoz� a vett adatokhoz
	SPID_DATA = 0xff;			//SPI olvas�s ind�t�sa
}

//Struktura a gomboknak
struct buttons {
	unsigned int Restart:5;			//Restart sz�ml�l�
	unsigned int RestartPushed:1;	//Restrat flag
	unsigned int Tensec:5;			//10 m�sodperc sz�ml�l�
	unsigned int TensecPushed:1;	//10 m�sodperc flag
	unsigned int Minute:5;			//1 perc sz�ml�l�
	unsigned int MinutePushed:1;	//1 perc flag
	unsigned int Tenmin:5;			//10 perc sz�ml�l�
	unsigned int TenminPushed:1;	//10 perc flag
	unsigned int Hour:5;			//1 �ra sz�ml�l�
	unsigned int HourPushed:1;		//1 �ra flag
	unsigned int Start:5;			//Start sz�ml�l�
	unsigned int StartPushed:1;		//Start flag
	unsigned int state:8;			//st�tusz
	unsigned int longPush:11;		//Hossz� lenyom�s
};


//Gombok olvas�sa
void readButtons(struct buttons* btn) {
	char buttonsInput = 0;		//V�ltoz� ami a gombokhoz tartoz� port beolvas�s�nak �rt�k�t t�rolja
	buttonsInput = ~(PORTE_IN & 0b00111111);	//Gombok beolvas�sa negelva (felh�z�s miatt)
	//Ha a restart gombot magasnak �rz�keli, de a hozz� tartoz� sz�ml�l� m�g nem �rte el a 25-�t
	if((buttonsInput & BTNRESTART) && btn->Restart < 25) {
		++btn->Restart;					//restart gombhoz tartoz� sz�ml�l� n�vel�se
		//Ha a restart gombhoz tartoz� sz�ml�l� �rt�ke nagyobb mint h�sz
		if(btn->Restart > 20) {
			btn->RestartPushed = 1;		//A gombot lenyomotnaK �rz�keli
		}
	}
	//Ha a alacsonynak �rz�keli �s a hozz� tartoz� stz�ml�l� nagyobb mint nulla
	else if(btn->Restart > 0){
		--btn->Restart;
		if(btn->Restart == 0) {
			btn->RestartPushed = 0;
		}
	}
	if((buttonsInput & BTNTENSEC) && btn->Tensec < 25) {
		++btn->Tensec;
		if(btn->Tensec > 20)  {
			btn->TensecPushed = 1;
		}
	}
	else if(btn->Tensec > 0) {
		--btn->Tensec;
		if(btn->Tensec == 0) {
			btn->TensecPushed = 0;
		}
	}
	if((buttonsInput & BTNMINUTE) && btn->Minute < 25) {
		++btn->Minute;
		if(btn->Minute > 20) {
			btn->MinutePushed = 1;
		}
	}
	else if(btn->Minute > 0) {
		--btn->Minute;
		if(btn->Minute == 0) {
			btn->MinutePushed = 0;
		}
	}
	if((buttonsInput & BTNTENMIN) && btn->Tenmin < 25) {
		++btn->Tenmin;
		if(btn->Tenmin > 20) {
			btn->TenminPushed = 1;
		}
	}
	else if(btn->Tenmin > 0) {
		--btn->Tenmin;
		if(btn->Tenmin == 0) {
			btn->TenminPushed = 0;
		}
	}
	if((buttonsInput & BTNHOUR) && btn->Hour < 25){
		++btn->Hour;
		if(btn->Hour > 20) {
			btn->HourPushed = 1;
		}
	}
	else if(btn->Hour > 0) {
		--btn->Hour;
		if(btn->Hour == 0) {
			btn->HourPushed = 0;
		}
	}
	if((buttonsInput & BTNSTART) && btn->Start < 25) {
		++btn->Start;
		if(btn->Start > 20) {
			btn->StartPushed = 1;
		}
	}
	else if(btn->Start > 0) {
		--btn->Start;
		if(btn->Start == 0) {
			btn->StartPushed = 0;
		}
	}
}

//Gombnyom�s kezel�se
//Visszak�ld egy jelet ha volt �rv�nyes gombnyom�s parancs
unsigned char buttonPsuhHandler(struct buttons* btn) {
	unsigned char btnSignal = IDLE;	//Visszat�r�si �rt�k: alap�rtelmez�sben idle
	switch(btn->state) {
		//Ha az el�z� �llapot gomb �llapot IDLE volt, akkor megvizsg�lva, hogy
		//az�ta t�rt�nt-e gombnyom�s. Ha t�rt�nt gombnyom�s aszerint v�ltoztatja az �rt�ket
		case BTNIDLE : {
			btn->longPush = 0;
			if(btn->RestartPushed) {
				btn->state = BTNRESTART;
			}
			else if(btn->StartPushed) {
				btn->state = BTNSTART;
			}
			else if(btn->TensecPushed) {
				btn->state = BTNTENSEC;
			}
			else if(btn->MinutePushed) {
				btn->state = BTNMINUTE;
			}
			else if(btn->TenminPushed) {
				btn->state = BTNTENMIN;
			}
			else if(btn->HourPushed) {
				btn->state = BTNHOUR;
			}
		} break;
		//Restart gomb van lenyomva
		case BTNRESTART	: {
				//Ha m�r nincs lenyomva
				if(!btn->RestartPushed) {
					btnSignal = BTNRESTART;	//K�ld egy jelet, hogy a restart gomb nyom�s t�rt�nt
					btn->state = BTNIDLE;	//Visszat�r a kiindul �llapotba
				}
			} break;
		//Start gomb van lenyomva	
		case BTNSTART	: {
				//Ha m�r nincs lenyomva
				if(!btn->StartPushed) {
					btnSignal = BTNSTART;	//K�ld egy jelet, hogy a start gomb nyom�s t�rt�nt
					btn->state = BTNIDLE;	//Visszat�r a kiindul �llapotba
				}
			} break;
		//Ha a 10 m�sodperc gomb van lenyomva
		case BTNTENSEC	: {
				//Ha m�r nincs lenyomva
				if(!btn->TensecPushed) {
					btnSignal = BTNTENSEC;	//K�ld egy jelet, hogy a start gomb nyom�s t�rt�nt
					btn->state = BTNIDLE;	//Visszat�r a kiindul �llapotba
				}
				//Ha tov�bbra is le van nyomva a gomb
				else {
					++btn->longPush;	//Sz�mlal� n�vel�se, amivel a hossz� lenyom�st �rz�keli
					//Ha k�r�lbel�l 1 m�sodpercig le volt nyomva
					if(btn->longPush == 1000) {
						btnSignal = BTNLONG1;	//Jelzi, hogy melyik gomb volt hosszan lenyomva
						btn->state = BTNLONG;	//Hossz� lenyom�s �llapotba ker�l
					}
				}
			} break;
		case BTNMINUTE	: {
				if(!btn->MinutePushed) {
					btnSignal = BTNMINUTE;
					btn->state = BTNIDLE;
				}
				else {
					++btn->longPush;
					if(btn->longPush == 1000) {
						btnSignal = BTNLONG2;
						btn->state = BTNLONG;
					}
				}
			} break;
		case BTNTENMIN	: {
				if(!btn->TenminPushed) {
					btnSignal = BTNTENMIN;
					btn->state = BTNIDLE;
				}
				else {
					++btn->longPush;
					if(btn->longPush == 1000) {
						btnSignal = BTNLONG3;
						btn->state = BTNLONG;
					}
				}
			} break;
		case BTNHOUR	: {
				if(!btn->HourPushed) {
					btnSignal = BTNHOUR;
					btn->state = BTNIDLE;
				}
				else {
					++btn->longPush;
					if(btn->longPush == 1000) {
						btnSignal = BTNLONG4;
						btn->state = BTNLONG;
					}
				}
			} break;
		//Miut�n �rz�kelt egy hossz� lenyom�st
		case BTNLONG	: {
			//Addig v�r, ameddig egyig gomb se lesz lenyomva
			if((!btn->TensecPushed) && (!btn->MinutePushed) && (!btn->TenminPushed) && (!btn->HourPushed))
				btn->state = BTNIDLE;	//Visszat�r a kiindul� �llapotba
		} break;
	}
	return btnSignal;	//Visszat�r a gombok �llapota szerint egy jelz�ssel
}

//Visszasz�ml�l� f�ggv�ny
void countDown(struct TimerDateStruct* timer) {
	//Ha a m�sodperc 0
	if(timer->sec == 0) {
		//Ha a perc is nulla
		if(timer->min == 0) {
			--timer->hour;		//�ra cs�kkent�se 1-el
			timer->min = 59;	//Perc �tfordul�s
			timer->sec = 59;	//M�sodperc �tfordul�s
		}
		//Ha csak a m�sodperc nulla
		else {
			--timer->min;		//Perc cs�kkent�se
			timer->sec = 59;	//M�sodperc �tfordul�s
		}
	}
	//Ha egyik �rt�k sem nulla
	else {
		--timer->sec;			//M�sodperc cs�kkent�se
	}
}

//D�tum l�ptet�se
//Ha eltelt egy m�sodperc a f�ggv�ny friss�ti az LCD kijelz�n a d�tumot
void incrementDate(struct DateStruct* GlobalDate) {
	char outDate[23] = "";		//Kimeneti karakter t�mb
	++GlobalDate->second;		//1/1024-ed m�sodperc n�vel�se
	//Ha eltelt egy m�sodperc
	if(!(GlobalDate->second & MSECOND_bm)) {
		flag.secondFlag = 1;	//Jelz�s, hogy eltelt egy m�sodperc
		//Ha a m�sodperc el�rte a 60-at
		if((GlobalDate->second & SECOND_bm) == 0xF000) {
			GlobalDate->second = 0;		//M�sodperc vissza�ll�t�sa 0-ba (�tfordul�s)
			++GlobalDate->dayHourMin;	//Perc n�vel�se
			//Ha a perc el�rte a 60-at
			if((GlobalDate->dayHourMin & MIN_bm) == 60) {
				GlobalDate->dayHourMin += 0x4;	//Perc vissza�ll�t�sa 0-ba (�tfordul�s) �s �ra n�vel�se
				//Ha az �ra el�rte a 24-et
				if((GlobalDate->dayHourMin & HOUR_bm) == 0x600) {
					GlobalDate->dayHourMin += 0x200;		//�ra vissza�ll�t�sa 0-ba (�tfordul�s) �s nap n�vel�se
					//Ha a nap el�rte a 30-at
					if((GlobalDate->dayHourMin & DAY_bm) == 0xF800) {
						++GlobalDate->yearMonth;	//H�nap n�vel�se
						GlobalDate->dayHourMin = (GlobalDate->dayHourMin & (HOUR_bm | MIN_bm)) + 0x800;	//Nap be�ll�t�sa 1-be
						//Ha a h�nap el�rte a 13-at
						if((GlobalDate->yearMonth & MONTH_bm) == 13) {
							GlobalDate->yearMonth += 0x4;	//H�nap vissza�ll�t�sa 1-be, �v l�ptet�se
							
						}
					}
				}
			}
		}
		dateToString(outDate, *GlobalDate);	//D�tum konvert�l�sa stringbe
		writeLCDdataToPos(outDate, 0x00);	//D�tum ki�r�sa LCD-re
	}
}

//H�m�rs�klet adat �tv�lt�sa
void convertTempAndWrite(int tmp) {
	char outTMP[10] = "";
	char fraction;
	unsigned char pos = 0;	//outTemp �r�s�hoz
	tmp >>= 3;		//Als� h�rom felesleges bit kishiftel�se
	fraction = tmp & 0b00001111;	//H�m�rs�klet t�rt r�sze
	//T�rt r�sz �tv�lt�sa
	switch(fraction) {
		case 0 : fraction = 0; break;
		case 1 : fraction = 1; break;
		case 2 : fraction = 1; break;
		case 3 : fraction = 2; break;
		case 4 : fraction = 3; break;
		case 5 : fraction = 3; break;
		case 6 : fraction = 4; break;
		case 7 : fraction = 4; break;
		case 8 : fraction = 5; break;
		case 9 : fraction = 6; break;
		case 10 : fraction = 6; break;
		case 11 : fraction = 7; break;
		case 12 : fraction = 8; break;
		case 13 : fraction = 8; break;
		case 14 : fraction = 9; break;
		case 15 : fraction = 9; break;
	}
	tmp >>= 4;		//T�rtr�sz kishiftel�se
	tmp -= 2;		//Kalibr�l�s
	//Ha negat�v, akkor visszalak�t�s
	if(tmp & 0x100) {
		tmp += 1;
		tmp = ~tmp;
		outTMP[pos] = '-';	//El� digit a m�nusz szimb�lum
		++pos;
	}
	else if (((tmp & 0xff) / 100) == 1) {
		outTMP[pos] = 1 + 48;	//Sz�zas helyi�rt�k
		++pos;
	}
	outTMP[pos++] = ((tmp & 0xff) / 10) + 48;
	outTMP[pos++] = ((tmp & 0xff) % 10) + 48;
	outTMP[pos++] = ',';
	outTMP[pos++] = fraction + 48;
	outTMP[pos++] = 0b11011111;	//fok
	outTMP[pos++] = 'C';
	outTMP[pos++] = '\r';
	outTMP[pos++] = '\n';
	outTMP[pos++] = '\0';
	//writeTextUART(outTMP);
	writeLCDdataToPos(outTMP, 0xce);
}


//RTC interrupt
ISR(RTC_OVF_vect) {
	flag.miliSecondFlag = 1;	//Eltelt 1/1024 m�sodperc
}

//SPI interrupt
ISR(SPID_INT_vect) {
	cli();
	inSPI |= SPID_DATA;		//SPI input regiszter olvas�sa
	//Ha m�r a m�sodik byte-ot vette
	if(!shiftSPI) {
		PORTD_OUTSET = 0b00000001;	//Chip enable  kikapcsol�sa
		SPID_INTCTRL = 0;			//SPI kikpacsol�sa
		flag.SPI_rdy = 1;			//Jelz�s, hogy van �rv�nyes SPI adat
	}
	//Ha az els� byte-ot vette
	else {
		inSPI <<= shiftSPI;		//Beolvasott adat shiftel�se 8-al
		shiftSPI = 0;			//Jelz�s, hogy vette az els� byte-ot
		SPID_DATA = 0xff;		//�jabb ciklus elkezd�se
	}
	sei();
}

volatile char USARTinData = 0;

//UART byte v�tel interrupt
ISR(USARTD0_RXC_vect) {
	USARTinData = USARTD0_DATA;		//Adat beolvas�sa
	flag.uartInputFlag = 1;			//Jelz�s, hogy az adat be lett olvasva
}

volatile unsigned int dcfLowCounter	= 0;
volatile unsigned int dcfHighCounter	= 0;
volatile char dcfEdge = 0;

void writeDCFTimerDataUART(unsigned int data) {
	char outDataDCF[10] = "";
	outDataDCF[0] = (data/10000) + 48;
	outDataDCF[1] = ((data/1000) % 10) + 48;
	outDataDCF[2] = (((data/100) % 100) % 10) + 48;
	outDataDCF[3] = (((data % 1000) % 100)/10) + 48;
	outDataDCF[4] = (data % 10) + 48;
	outDataDCF[5] = '\r';
	outDataDCF[6] = '\n';
	outDataDCF[7] = '\0';
	writeTextUART(outDataDCF);
}

//DCF77 �l�rz�kel�s
ISR(PORTB_INT0_vect) {
	cli();
	//Ha felfut� �l j�tt
	if(dcfEdge) {
		dcfLowCounter = TCC1_CNT;		//Sz�ml�l� tartalm�nak olvas�sa
		dcfEdge = 0;					//Jelz�s, hogy a k�vetket� interrupt lefut� �lre j�n
		TCC1_CNT = 0;					//Sz�ml�l� t�rl�se
		writeDCFTimerDataUART(TCC1_CNT);
		PORTB_INTFLAGS	= 0b00000011;	//Interrupt t�rl�se
		PORTB_PIN3CTRL	= 0b00000010;	//Interrupt lefut� �lre
		PORTB_OUTTGL = 0b01100000;
		flag.dcfLowInputFlag = 1;		//Sz�ml�l�s befejez�se
		writeTextUART("RI\0");
	}
	//Lefut� �ln�l
	else {
		dcfHighCounter	= TCC1_CNT;		//Sz�ml�l� tartalm�nak olvas�sa
		dcfEdge = 1;					//Jelz�s, hogy a k�vetkez� interrupt felfut� �lre j�n
		TCC1_CNT = 0;					//Sz�ml�l� elind�t�sa
		PORTB_INTFLAGS	= 0b00000011;	//Interrupt t�rl�se
		PORTB_PIN3CTRL	= 0b00000001;	//K�vetkez� interrtupt felfut� �lre
		flag.dcfHighInputFlag = 1;		//Sz�ml�l�s befejez�se
		writeTextUART("Fl\0");
	}
	sei();
}


//IO portok inicializ�l�sa: adatir�ny, felh�z� ellen�ll�s, kezd��rt�k
void initIO() {
	//PORTA
	PORTA_DIR |= 0b00000000;
	
	//PORTB
	PORTB_DIR		|= 0b01110111;
	PORTB_PIN0CTRL	|= 0b01000000;	//Port m�k�d�s�nek invert�l�sa, hogy j� legyen a logika
	PORTB_PIN1CTRL	|= 0b01000000;	//Port m�k�d�s�nek invert�l�sa, hogy j� legyen a logika
	PORTB_PIN2CTRL	|= 0b01000000;	//Port m�k�d�s�nek invert�l�sa, hogy j� legyen a logika
	
	//PORTC
	PORTC_DIR		|= 0b00000000;
	
	//PORTD
	PORTD_DIR		|= 0b10011001;
	PORTD_OUTSET	 = 0b00000001;
	
	//PORTE
	PORTE_DIR		|= 0b00000000;	//PE0-PE5 bemenetek
	PORTE_PIN0CTRL	|= 0b00011000;	//PE0 pullup enged�lyez�s
	PORTE_PIN1CTRL	|= 0b00011000;	//PE1 pullup enged�lyez�s
	PORTE_PIN2CTRL	|= 0b00011000;	//PE2 pullup enged�lyez�s
	PORTE_PIN3CTRL	|= 0b00011000;	//PE3 pullup enged�lyez�s
	PORTE_PIN4CTRL	|= 0b00011000;	//PE4 pullup enged�lyez�s
	PORTE_PIN5CTRL	|= 0b00011000;	//PE5 pullup enged�lyez�s
	
	//PORTF
	PORTF_DIR	|= 0b00000001;
	PORTF_OUT	 = 0b00000000;
}

//Real Time Clock inicializ�l�sa
void initRTC() {
	CCP = CCP_IOREG_gc;
	OSC_CTRL	|= 0b00000100;
	CLK_RTCCTRL |= 0b00001101;	//RTC enged�lyez�se 1,024 kHz-es bels� oszcill�torr�l
	RTC_CTRL	|= 0b00000001;	//Nincs prescaling --> 1,024 kHz-es frekvencia
	while((RTC_STATUS & RTC_SYNCBUSY_bm) || !(OSC_STATUS & OSC_RC32KRDY_bm));
	RTC_PER		 = 31;
	RTC_INTCTRL	|= 0b00000011;	//Overflow interrupt enged�lyezve magas priorit�ssal
	RTC_CNT		 = 0;
	RTC_COMP	 = 4;
	
};

//DCF77 vev�h�z id�z�t�
void initDCFtimer() {
	TCC1_CTRLA |= 0b00000101;	//clk/64
	TCC1_CTRLB |= 0;	//sima sz�ml�l�s
	TCC1_CTRLC |= 0;	//sima sz�ml�l�s
	TCC1_CTRLD |= 0;	//sima sz�ml�l�s
	TCC1_CTRLE |= 0;	//sima sz�ml�l�s
	TCC1_PER   |= 32700;	//kicsivel t�bb, mint 1 mp
	dcfEdge = 1;					//K�vetkez� interrupt ut�n felfut� �l
	PORTB_PIN3CTRL	 = 0b00000010;	//Nincs invert�l�s, nincs fel vagy leh�z�s, interrupt felfut� �l
	PORTB_INTCTRL	|= 0b00000011;	//lvl 0 interrupt enged�lyez�s
	PORTB_INT0MASK	|= 0b00001000;	//Interrupt enged�lyez�s PB3-on
}

void initSPI() {
	SPID_CTRL = 0b01011100;		//Enable SPI as master, clk/4
}

void initBuzzer() {
	TCC0_CTRLA  = 0b00000001;	//2MHz
	TCC0_CTRLB  = 0b01000001;	//Frequency generation
	TCC0_CCA	= 599;			//1kHz
	
}

//Inicializ�ci�s f�ggv�ny, ami elindul�skor h�v�dik meg
//Ez h�vja meg az egyes egys�gek inicializ�ci�j�t
void init() {
	initIO();
	initLCD();
	initRTC();
	initUART();
	initSPI();
	initDCFtimer();
	initBuzzer();
}

//Bej�v� UART adatok feldolgoz�sa
void processUARTData(char data, struct uartState* stateUART, struct DateStruct* GlobalDate, struct ButtonPreset* buttonPreset) {
	switch(stateUART->state) {
		case IDLE : {
			switch(data) {
				case 'E' : stateUART->state = YEARSET; break;
				case 'H' : stateUART->state = MONTHSET; break;
				case 'N' : stateUART->state = DAYSET; break;
				case 'O' : stateUART->state = HOURSET; break;
				case 'P' : stateUART->state = MINSET; break;
				case 'M' : stateUART->state = SECSET; break;
				case 'T' : stateUART->state = BUTTONSET; break;
			}
			stateUART->inCounter = 0;
		} break;
		case YEARSET : {
			PORTB_OUTCLR = 0b01000000;
			PORTB_OUTSET = 0b00100000;
			switch(stateUART->inCounter) {
				case 0 : stateUART->dateDate = 1000*(data-48); ++stateUART->inCounter; break;
				case 1 : stateUART->dateDate += 100*(data-48); ++stateUART->inCounter; break;
				case 2 : stateUART->dateDate += 10*(data-48); ++stateUART->inCounter; break;
				case 3 : {
					stateUART->dateDate += (data-48);
					GlobalDate->yearMonth = (stateUART->dateDate << 4) | (GlobalDate->yearMonth & MONTH_bm);
					stateUART->state = IDLE;
					PORTB_OUTCLR = 0b00100000;
					PORTB_OUTSET = 0b01000000;
				} break;
			}
		} break;
		case MONTHSET : {
			PORTB_OUTCLR = 0b01000000;
			PORTB_OUTSET = 0b00100000;
			switch(stateUART->inCounter) {
				case 0 : stateUART->dateDate = 10*(data-48); ++stateUART->inCounter; break;
				case 1 : {
					stateUART->dateDate += (data-48); ++stateUART->inCounter;
					GlobalDate->yearMonth = (stateUART->dateDate & MONTH_bm) | (GlobalDate->yearMonth & YEAR_bm);
					stateUART->state = IDLE; PORTB_OUTCLR = 0b00100000; PORTB_OUTSET = 0b01000000;
				} break;
			}
		} break;
		case DAYSET : {
			PORTB_OUTCLR = 0b01000000;
			PORTB_OUTSET = 0b00100000;
			switch(stateUART->inCounter) {
				case 0 : stateUART->dateDate = 10*(data-48); ++stateUART->inCounter; break;
				case 1 : {
					stateUART->dateDate += (data-48); ++stateUART->inCounter;
					GlobalDate->dayHourMin = (stateUART->dateDate << 11) | (GlobalDate->dayHourMin & (HOUR_bm | MIN_bm));
					stateUART->state = IDLE; PORTB_OUTCLR = 0b00100000; PORTB_OUTSET = 0b01000000;
				} break;
			}
		} break;
		case HOURSET : {
			PORTB_OUTCLR = 0b01000000;
			PORTB_OUTSET = 0b00100000;
			switch(stateUART->inCounter) {
				case 0 : stateUART->dateDate = 10*(data-48); ++stateUART->inCounter; break;
				case 1 : {
					stateUART->dateDate += (data-48); ++stateUART->inCounter;
					GlobalDate->dayHourMin = (stateUART->dateDate << 6) | (GlobalDate->dayHourMin & (DAY_bm | MIN_bm));
					stateUART->state = IDLE; PORTB_OUTCLR = 0b00100000; PORTB_OUTSET = 0b01000000;
				} break;
			}
		} break;
		case MINSET : {
			PORTB_OUTCLR = 0b01000000;
			PORTB_OUTSET = 0b00100000;
			switch(stateUART->inCounter) {
				case 0 : stateUART->dateDate = 10*(data-48); ++stateUART->inCounter; break;
				case 1 : {
					stateUART->dateDate += (data-48); ++stateUART->inCounter;
					GlobalDate->dayHourMin = stateUART->dateDate | (GlobalDate->dayHourMin & (DAY_bm | HOUR_bm));
					stateUART->state = IDLE; PORTB_OUTCLR = 0b00100000; PORTB_OUTSET = 0b01000000;
				} break;
			}
		} break;
		case SECSET : {
			PORTB_OUTCLR = 0b01000000;
			PORTB_OUTSET = 0b00100000;
			switch(stateUART->inCounter) {
				case 0 : stateUART->dateDate = 10*(data-48); ++stateUART->inCounter; break;
				case 1 : {
					stateUART->dateDate += (data-48); ++stateUART->inCounter;
					GlobalDate->second = (stateUART->dateDate << 10) | (GlobalDate->second & MSECOND_bm);
					stateUART->state = IDLE; PORTB_OUTCLR = 0b00100000; PORTB_OUTSET = 0b01000000;
				} break;
			}
		} break;
		case BUTTONSET : {
			if(stateUART->inCounter == 7) {
				switch((unsigned)(data-48)) {
					case 1 : setTimerDateSet(&buttonPreset->btnTenSec, stateUART->textBuf);
					case 2 : setTimerDateSet(&buttonPreset->btnMinute, stateUART->textBuf);
					case 3 : setTimerDateSet(&buttonPreset->btnTenMin, stateUART->textBuf);
					case 4 : setTimerDateSet(&buttonPreset->btnHour, stateUART->textBuf);
				}
				stateUART->state = IDLE;
			}
			else {
				stateUART->textBuf[stateUART->inCounter++] = data;
			}
		}
	}
}

void mainButtonsState(unsigned char* state, struct buttons* btn, struct TimerDateStruct* timer, char* countDownFlag, struct DateStruct* GlobalDate, struct ButtonPreset* buttonPreset) {
	switch(*state) {
		case READY : {
			switch(buttonPsuhHandler(btn)) {
				case BTNRESTART : *state = TIMESET; GlobalDate->dateSetState = YEARSET; break;
				case BTNSTART	: {
					timer->hour = 0;
					timer->min = 0;
					timer->sec = 0;
					writeTimerToLCD(timer);
					*state = TIMERSET;
				} break;
			}
		} break;
		case TIMESET : {
			switch(buttonPsuhHandler(btn)) {
				case BTNRESTART : {
					GlobalDate->dateSetState = IDLE;
					*state = READY;
				} break;
			}
		} break;
		case TIMERSET : {
			switch(buttonPsuhHandler(btn)) {
				case BTNRESTART : *state = READY; writeLCDdataToPos("       \r\n\0", 0x9A); break;
				case BTNTENSEC	: {
					timer->sec += 10;
					if(timer->sec == 60)
						timer->sec = 0;
					writeTimerToLCD(timer);
				} break;
				case BTNMINUTE	: {
					timer->min += 1;
					if(timer->min == 60)
						timer->min = 0;
					writeTimerToLCD(timer);
				} break;
				case BTNTENMIN	: { 
					(timer->min >= 50) ? (timer->min = 0) : (timer->min += 10);
					writeTimerToLCD(timer);
				} break;
				case BTNHOUR	: {
					timer->hour +=  1;
					if(timer->hour == 10)
						timer->hour = 0;
					writeTimerToLCD(timer);
				} break;
				case BTNLONG1	: cpyTimerDate(timer, &buttonPreset->btnTenSec); writeTimerToLCD(timer); break;
				case BTNLONG2	: cpyTimerDate(timer, &buttonPreset->btnMinute); writeTimerToLCD(timer); break;
				case BTNLONG3	: cpyTimerDate(timer, &buttonPreset->btnTenMin); writeTimerToLCD(timer); break;
				case BTNLONG4	: cpyTimerDate(timer, &buttonPreset->btnHour); writeTimerToLCD(timer); break;
				case BTNSTART	: *state = COUNTDOWN; break;
			}
		} break;
		case COUNTDOWN : {
			if(*countDownFlag) {
				*countDownFlag = 0;
				countDown(timer);
				writeTimerToLCD(timer);
				if((timer->hour == 0) && (timer->min == 0) && (timer->sec == 0)) {
					*state = BEEP;
				}
				if(buttonPsuhHandler(btn) == BTNRESTART) {
					*state = READY;
					writeLCDdataToPos("       \r\n\0", 0x9A); break;
				}
			}
		} break;
		case BEEP : {
			if(*countDownFlag) {
				*countDownFlag = 0;
				PORTB_OUTTGL = 0b00010000;
				PORTC_DIRTGL = 0b00000100;
			}
			if(buttonPsuhHandler(btn) && (BTNHOUR | BTNMINUTE | BTNTENMIN | BTNTENSEC | BTNRESTART | BTNSTART)) {
				*state = READY;
				PORTB_OUTCLR = 0b00010000;
				PORTB_OUTSET = 0b01000000;
				PORTC_DIRCLR = 0b00000100;
				writeLCDdataToPos("       \r\n\0", 0x9A); break;
			}
		} break;
	}
}

int main(void)
{
	//UART bej�v� adat olvas�s�hot �llapot g�p
	struct uartState stateUART; //TODO ha van DCF77 meg kell sz�ntetni a kommentet
	stateUART.state = IDLE;
	//Rendszerid�
	struct DateStruct GlobalDate;
	//Gombok kezel�se
	unsigned char state = READY;
	struct buttons btn;
	btn.state = BTNIDLE;
	struct ButtonPreset buttonPreset;
	setTimerDateSet(&buttonPreset.btnTenSec,"0:02:30");
	setTimerDateSet(&buttonPreset.btnMinute,"0:15:00");
	setTimerDateSet(&buttonPreset.btnTenMin,"0:30:00");
	setTimerDateSet(&buttonPreset.btnHour,"1:00:00");
	//Id�z�t�
	struct TimerDateStruct timer;
	char countDownFlag = 0;
	//DCF77
	char statusDCF = DCFREADY;
	char dcfInputData = 0;
	unsigned int dcfTimeData = 0;
	unsigned char dcfInputCounter = 0;
	char parity = 0;
	int tmp;
	init();
	GlobalDate = setDate(0xff,0xff);	//Alap�rtelmezett indul�si id� be�ll�t�sa
	PMIC_CTRL |= 0b00000111;	//Minden t�pus� interrupt enged�lyez�se
	sei();
	PORTB_OUT |= 0b01000000;
	while(1) {
		if(flag.SPI_rdy | (SPID_STATUS & (1 << 7))) {
			tmp = inSPI;
			flag.SPI_rdy = 0;
			convertTempAndWrite(tmp);
		}
		
		//Miliszekundumok
		if(flag.miliSecondFlag) {
			flag.miliSecondFlag = 0;
			incrementDate(&GlobalDate);
			readButtons(&btn);
			mainButtonsState(&state, &btn, &timer, &countDownFlag, &GlobalDate, &buttonPreset);
		}
		
		//M�sodpercek
		if(flag.secondFlag) {
			flag.secondFlag = 0;
			if((state == COUNTDOWN) || (state == BEEP) || (state == TIMESET))  {
				countDownFlag = 1;
			}
			readTMP();
		}
		
		
		if(flag.dcfHighInputFlag || flag.dcfLowInputFlag) {
			if(flag.dcfLowInputFlag && ((dcfLowCounter > 2800) && (dcfLowCounter < 3500))) {
				flag.dcfLowInputFlag = 0;
				dcfLowCounter = 0;
				dcfInputData = DCFZERO;
				writeTextUART("0\0");
			}
			else if(flag.dcfLowInputFlag && ((dcfLowCounter > 5900) && (dcfLowCounter < 6600))) {
				flag.dcfLowInputFlag = 0;
				dcfLowCounter = 0;
				dcfInputData = DCFONE;
				writeTextUART("1\0");
			}
			else if(flag.dcfHighInputFlag && (dcfHighCounter > 31000)) {
				flag.dcfHighInputFlag = 0;
				dcfHighCounter = 0;
				dcfInputData = DCFENDMINUTE;
				minuteMarker(GlobalDate);
			}
			else {
				//Ha lefut� �l j�tt, akkor az �rv�nytelen adat nem sz�m�t
				if(flag.dcfHighInputFlag) {
					flag.dcfHighInputFlag = 0;
					//Ha �pp v�tel van, akkor vissza kell �ll�tani a sz�ml�l�t eggyel
					//K�l�nben a magas szintet is adatnak �rz�keln�
					if(dcfInputCounter > 0) {
						--dcfInputCounter ;
					}	
				}
				writeDCFTimerDataUART(dcfLowCounter);
				flag.dcfLowInputFlag = 0;
				dcfInputData = DCFINVALID;
			}
			switch(statusDCF) {
				case DCFREADY : {
					if(dcfInputData == DCFENDMINUTE) {
						statusDCF = DCFSTART;
						dcfInputCounter = 0;
					}
				} break; 
				case DCFSTART : {
					if((dcfInputCounter == 0) && (dcfInputData != DCFZERO)) {
						statusDCF = DCFREADY;	//Az els� bit mindig nulla
						writeTextUART("First bit error\0");
					}
					if((dcfInputCounter == 20) && (dcfInputData == DCFONE)) {
						statusDCF = DCFMINUTE;	//A 21-ik bit mindig 1
						writeTextUART("DCF minute\0");
						dcfTimeData = 0;
						parity = 0;
					}
					else if((dcfInputCounter == 20) && (dcfInputData != DCFONE)) {
						statusDCF = DCFREADY;
						writeTextUART("MarkerError\0");
					}
					++dcfInputCounter;
				} break;
				case DCFMINUTE : {
					switch(dcfInputCounter) {
						case 21 : if(dcfInputData == DCFONE) { dcfTimeData += 1;  parity ^= 1;} break;
						case 22 : if(dcfInputData == DCFONE) { dcfTimeData += 2;  parity ^= 1;} break;
						case 23 : if(dcfInputData == DCFONE) { dcfTimeData += 4;  parity ^= 1;} break;
						case 24 : if(dcfInputData == DCFONE) { dcfTimeData += 8;  parity ^= 1;} break;
						case 25 : if(dcfInputData == DCFONE) { dcfTimeData += 10; parity ^= 1;} break;
						case 26 : if(dcfInputData == DCFONE) { dcfTimeData += 20; parity ^= 1;} break;
						case 27 : if(dcfInputData == DCFONE) { dcfTimeData += 30; parity ^= 1;} break;
						case 28 : {
							if(((dcfInputData == DCFZERO) && (!parity)) || ((dcfInputData == DCFONE) && (parity))) {
								setDate(MINSET, dcfTimeData);
							}
							statusDCF = DCFHOURS;
							writeTextUART("DCF hours");
							parity = 0;
							dcfTimeData = 0;
						} break;
					}
					++dcfInputCounter;
				} break;
				case DCFHOURS : {
					switch(dcfInputCounter) {
						case 29 : if(dcfInputData == DCFONE) { dcfTimeData += 1; parity ^= 1;} break;
						case 30 : if(dcfInputData == DCFONE) { dcfTimeData += 2; parity ^= 1;} break;
						case 31 : if(dcfInputData == DCFONE) { dcfTimeData += 4; parity ^= 1;} break;
						case 32 : if(dcfInputData == DCFONE) { dcfTimeData += 8; parity ^= 1;} break;
						case 33 : if(dcfInputData == DCFONE) { dcfTimeData += 10; parity ^= 1;} break;
						case 34 : if(dcfInputData == DCFONE) { dcfTimeData += 20; parity ^= 1;} break;
						case 35 : {
							if(((dcfInputData == DCFZERO) && (!parity)) || ((dcfInputData == DCFONE) && (parity)))
								setDate(HOURSET, dcfTimeData);
							statusDCF = DCFDAYOFMONTH;
							writeTextUART("DCF dayofmonth");
							parity = 0;
							dcfTimeData = 0;
						} break;
					}
					++dcfInputCounter;
				} break;
				case DCFDAYOFMONTH : {
					switch(dcfInputCounter) {
						case 36 : if(dcfInputData == DCFONE) { dcfTimeData += 1; parity ^= 1;} break;
						case 37 : if(dcfInputData == DCFONE) { dcfTimeData += 2; parity ^= 1;} break;
						case 38 : if(dcfInputData == DCFONE) { dcfTimeData += 4; parity ^= 1;} break;
						case 39 : if(dcfInputData == DCFONE) { dcfTimeData += 8; parity ^= 1;} break;
						case 40 : if(dcfInputData == DCFONE) { dcfTimeData += 10; parity ^= 1;} break;
						case 41 : {
							if(dcfInputData == DCFONE) { dcfTimeData += 10; parity ^= 1;}
							setDate(DAYSET, dcfTimeData);
							statusDCF = DCFDAY;
							writeTextUART("DCF day");
							dcfTimeData = 0;
						} break;
					}
					++dcfInputCounter;
				} break;
				case DCFDAY : {
					switch(dcfInputCounter) {
						case 42 : if(dcfInputData == DCFONE) { dcfTimeData += 1; parity ^= 1;} break;
						case 43 : if(dcfInputData == DCFONE) { dcfTimeData += 2; parity ^= 1;} break;
						case 44 : {
							if(dcfInputData == DCFONE) { dcfTimeData += 4; parity ^= 1;}
							statusDCF = DCFMONTH;
							writeTextUART("DCF month");
							dcfTimeData = 0;
						} break;
					}
					++dcfInputCounter;
				} break;
				case DCFMONTH : {
					switch(dcfInputCounter) {
						case 45 : if(dcfInputData == DCFONE) { dcfTimeData += 1; parity ^= 1;} break;
						case 46 : if(dcfInputData == DCFONE) { dcfTimeData += 2; parity ^= 1;} break;
						case 47 : if(dcfInputData == DCFONE) { dcfTimeData += 4; parity ^= 1;} break;
						case 48 : if(dcfInputData == DCFONE) { dcfTimeData += 8; parity ^= 1;} break;
						case 49 : {
							if(dcfInputData == DCFONE) { dcfTimeData += 10; parity ^= 1;}
							setDate(MONTHSET, dcfTimeData);
							statusDCF = DCFYEAR;
							writeTextUART("DCF year");
							dcfTimeData = 0;
						} break;
					}
					++dcfInputCounter;
				} break;
				case DCFYEAR : {
					switch(dcfInputCounter) {
						case 50 : if(dcfInputData == DCFONE) { dcfTimeData += 1; parity ^= 1;} break;
						case 51 : if(dcfInputData == DCFONE) { dcfTimeData += 2; parity ^= 1;} break;
						case 52 : if(dcfInputData == DCFONE) { dcfTimeData += 4; parity ^= 1;} break;
						case 53 : if(dcfInputData == DCFONE) { dcfTimeData += 8; parity ^= 1;} break;
						case 54 : if(dcfInputData == DCFONE) { dcfTimeData += 10; parity ^= 1;} break;
						case 55 : if(dcfInputData == DCFONE) { dcfTimeData += 20; parity ^= 1;} break;
						case 56 : if(dcfInputData == DCFONE) { dcfTimeData += 40; parity ^= 1;} break;
						case 57 : {
							if(dcfInputData == DCFONE) { dcfTimeData += 80; parity ^= 1;}
							setDate(YEARSET, dcfTimeData);
							statusDCF = READY;
							writeTextUART("DCF recieved");
							dcfTimeData = 0;
						} break;
					}
				} break;
			}
		}
		
		//Ha j�tt �j UART adat
		if(flag.uartInputFlag) {
			flag.uartInputFlag = 0;
			processUARTData(USARTinData, &stateUART, &GlobalDate, &buttonPreset);
		}
	}
}