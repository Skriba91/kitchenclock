/*
 * lcd.c
 *
 * Created: 2018. 05. 25. 1:28:00
 *  Author: DANI
 */ 


#include <avr/io.h>
#include "lcd.h"

#define F_CPU 2000000
#include <util/delay.h>
#include <avr/interrupt.h>

#define LCD_E			PIN1_bm
#define LCD_RS			PIN2_bm
#define LCD_RW			PIN0_bm
#define LCD_D0			PIN1_bm
#define BF				PIN7_bm
#define LCD_H_bm		0b11111110
#define LCD_L_bm		0b00000001
#define callByte		0x30
#define functionSet		0b00111000
#define displayOff		0b00001000
#define dispayClear		0b00000001	//Parancs byte a k�perny� t�rl�s�re
#define entryModeSet	0b00000110
#define displayOn		0b00001100
#define returnHome		0b00000010

void writeLCDcommand(char cmd) {
	//while(PORTF_IN & BF);					//Ameddig az LCD busy nincs �r�s
	PORTB_OUTCLR	= LCD_RW;				//LCD �r�s
	PORTF_DIRSET	= 0b11111110;			//DB7-DB1 kimenet F porton
	PORTA_DIRSET	= 0b00000010;			//DB1 kimenet A porton
	PORTB_OUTCLR	= LCD_RS;				//RS �s RW jelek 0 --> parancs �r�s
	PORTB_OUTSET	= LCD_E;				//E felfut�
	PORTF_OUTCLR	= LCD_H_bm;				//LCD fels� bitjeinek
	PORTF_OUTSET	= (cmd & LCD_H_bm);		//LCD fels� bitjeinek be�ll�t�sa
	if(cmd & LCD_L_bm)						//LCD als� bitj�nek �ll�t�sa
		PORTA_OUTSET = LCD_D0;
	else
		PORTA_OUTCLR = LCD_D0;
	PORTB_OUTCLR = LCD_E;					//LCD lefut�
	for(int i = 0; i < 2; ++i);
	PORTF_OUTCLR = LCD_H_bm;				//LCD fels� bitjeinek t�rl�se
	PORTA_OUTCLR = LCD_D0;					//D0 t�rl�se
	PORTF_DIRCLR = 0b11111110;				//DB7-DB1 bemenet F porton
	PORTA_DIRCLR = 0b00000010;				//DB1 bemenet A porton
	PORTB_OUTSET = LCD_RW;					//LCD olvas�s
}

void writeLCDdata(char data) {
	PORTB_OUTSET	= LCD_E;
	while(PORTF_IN & BF);					//Ameddig az LCD busy nincs �r�s
	PORTB_OUTCLR	= LCD_E;
	PORTB_OUTCLR	= LCD_RW;				//LCD �r�s
	PORTF_DIRSET	= 0b11111110;			//DB7-DB1 kimenmet F porton
	PORTA_DIRSET	= 0b00000010;			//DB1 kimenet A porton
	PORTB_OUTSET	= LCD_RS;				//Adat �r�s
	PORTB_OUTSET	= LCD_E;				//E felfut�
	PORTF_OUTCLR	= LCD_H_bm;				//LCD fels� bitjeinek t�rl�se
	PORTF_OUTSET	= (data & LCD_H_bm);	//Adat regiszter �r�sa els� iniciliz�l�si l�p�s
	if(data & LCD_L_bm)
		PORTA_OUTSET = LCD_D0;
	else
		PORTA_OUTCLR = LCD_D0;
	PORTB_OUTCLR	= LCD_E;				//E lefut�
	PORTF_DIRCLR = 0b11111110;				//DB7-DB1 bemenet F7-F1 porton
	PORTA_DIRCLR = 0b00000010;				//DB1 bemenet A1 porton
	PORTB_OUTCLR = LCD_RS;					//Ready bit olvas�s
	PORTB_OUTSET = LCD_RW;					//LCD olvas�s
}

//char lcdOutBuff[81];

void writeLCDdataToPos(char* data, char pos) {
	unsigned char n = 0;
	//Ha nincs c�m �r�s
	if(pos == 0xff) {
		while(data[n] != '\r') {
			writeLCDdata(data[n]);
			++n;
		}
	}
	else {
		writeLCDcommand(0x80 | pos);
		while(data[n] != '\r') {
			writeLCDdata(data[n]);
			++n;
		}
	}
}

//LCD inicializ�l�sa
void initLCD() {
	_delay_ms(50);
	PORTB_OUTSET	= LCD_RW;				//LCD olvas�s
	PORTF_OUTSET	= 0b00000001;			//H�tt�rvil�g�t�s bekapcsol�sa
	writeLCDcommand(functionSet);
	
	_delay_ms(5);
	writeLCDcommand(dispayClear);

	_delay_ms(5);
	writeLCDcommand(entryModeSet);
	
	_delay_ms(5);
	writeLCDcommand(displayOn);
}