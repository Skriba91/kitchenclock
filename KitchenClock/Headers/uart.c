#include "uart.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>


char UARToutBUffer[200] = "";	//Kimen� adat
int UARToutBufferIndex = 0;		//Poz�ci� index

//UART interf�szt inicia�iz�l� f�ggv�ny
void initUART() {
	USARTD0_CTRLA |= 0b00110000;	//Nincs UART interrupt
	USARTD0_CTRLB |= 0b00011000;	//UART ad� �s vev� enged�lyez�se
	USARTD0_CTRLC |= 0b00100011;	//Async, even, 1 stop
	USARTD0_BAUDCTRLA |= 12;		//
	USARTD0_BAUDCTRLB = 0;			//BAUD rate 9600
}

//Adat k�ld�s UART-on
void sendUART(char data) {
	USARTD0_DATA = data;
}

void writeTextUART(char text[200]) {
	strcpy(UARToutBUffer,text);	//Adat bem�sol�sa buffer regiszterbe
	USARTD0_CTRLA = 0b00000011;			//Adat regiszter �res IT
	
}

ISR(USARTD0_DRE_vect) {
	if(UARToutBUffer[UARToutBufferIndex] != '\0') {
		USARTD0_DATA = UARToutBUffer[UARToutBufferIndex];
		++UARToutBufferIndex;
	}
	else {
		USARTD0_CTRLA = 0b00110000;		//Nincs t�bb IT
		UARToutBufferIndex = 0;
		strcpy(UARToutBUffer,"");
	}
}



