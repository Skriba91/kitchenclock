/*
 * lcd.h
 *
 * Created: 2018. 05. 25. 1:27:42
 *  Author: DANI
 */ 


#ifndef LCD_H_
#define LCD_H_

extern void writeLCDcommand(char cmd);

extern void writeLCDdata(char data);

extern void writeLCDdataToPos(char* data, char pos);

//LCD inicializálása
void initLCD();



#endif /* LCD_H_ */