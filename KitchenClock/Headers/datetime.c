/*
 * datetime.c
 *
 * Created: 2018. 05. 27. 17:23:39
 *  Author: DANI
 */ 

#include "datetime.h"

#define MSECOND_bm	0b0000001111111111
#define SECOND_bm	0b1111110000000000
#define MIN_bm		0b0000000000111111
#define HOUR_bm		0b0000011111000000
#define DAY_bm		0b1111100000000000
#define MONTH_bm	0b0000000000001111
#define YEAR_bm		0b1111111111110000

struct DateStruct GlobalDate;
